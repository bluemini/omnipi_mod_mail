from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt

import json, os, time, email
import datetime as dt

from bs4 import BeautifulSoup as BeautifulSoup

import mod_mail.webmail as webmail
import mod_mail.setupmail as setupmail

import omnipi.omnipi as op
import omnipi.timezone as optz

from mod_mail.models import Email
from mod_mail.settings import MailSettings
import mailbox


# open the mail folder
mailboxPath = os.path.join(settings.OP_ROOT, "store/mail")
mailbox.Maildir.colon = '!'
mailBox = mailbox.Maildir(mailboxPath, factory=None)


def index(request):
    # allEmail = mailBox.items()
    allEmail = Email.objects.order_by('-email_date')[:20]
    context = {}
    context['AppName'] = "WebMail"
    context["appname"] = "mail"
    context['settings'] = True
    context['data'] = {}

    # pare down the email data to just what the template needs
    try:
        allmail = parseMail(allEmail)
    except:
        return setupmail.gotosetup()

    context['data']['mail'] = []
    for mailitem in allmail:
        context['data']['mail'].append({
            'mssg_id': mailitem['mssg_id'],
            'from_name': mailitem['from_name'],
            'subject': mailitem['subject'],
            'body': mailitem['body'][:100],
            'email_date': mailitem['email_date'],
            'uri': 'http://localhost:8001/{}/#{}'.format(context['appname'], mailitem['mssg_id']),
            })

    return render(request, 'mail/index.html', context)


def refresh(request, **args):
    print(args)
    
    new = 0
    old = 0

    if 'a' in request.GET:
        acc = request.GET['a']
        if acc == 'all':
            listAccounts = webmail.getAccounts()
        else:
            listAccounts = [acc]
    else:
        return HttpResponse('<html><body>You must supply an account to refresh</body></html>')

    for acc in listAccounts:
        print(acc)
        new,old = webmail.sync(acc, mailBox)

    return HttpResponse('<html><body>Check the console output...'+str(new)+' new...'+str(old)+' old</body></html>')


def sync(request, **args):
    webmail.syncMailWithDb(mailBox)
    return HttpResponse('<html><body>Sync complete..</body></html>')


def fetch(request, **args):
    '''From the inbox, the client will send the message id for a paricular message in
    order to view it. This will extract the message from the mailbox and return it as
    a JSON object. '''
    success = "true"
    message = ""
    emailJson = "[]"

    if 'id' in request.GET:
        messageId = request.GET['id'].encode('utf-8')

        # lookup the mailbox number based on the message id
        EmailData = Email.objects.filter(mssg_id=messageId)
        print("Messages found:", EmailData.count())

        # if we have a record in the db, extract it from the mailbox
        if EmailData.count() > 0:
            print("Retrieving message", EmailData[0].mssg_number, "from mailbox @", mailboxPath)
            emailData = mailBox.get(EmailData[0].mssg_number)

            # if we have a message object, we parse it into JSON
            if emailData != None:
                eml = parseMailboxMessage([(EmailData[0].mssg_number, emailData)])
                emailJson = json.dumps(eml[0])
            else:
                success = 'false'
                message = 'unable to retrieve message from mailbox'

        else:
            success = 'false'
            message = 'no message found that matches the specified id'

    else:
        success = 'false'
        message = 'The URL must contain an id argument with the messages id'

    return HttpResponse('{"success":' + success + ', "email":' + emailJson + ', "message": "' + message + '"}', content_type='application/json')

def fetchHtml(request, **args):
    '''From the inbox, the client will send the message id for a paricular message in
    order to view it. This will extract the message from the mailbox and return it as
    a JSON object. '''
    success = "true"
    message = ""
    context = {}

    if 'id' in request.GET:
        messageId = request.GET['id'].encode('utf-8')

        # lookup the mailbox number based on the message id
        EmailData = Email.objects.filter(mssg_id=messageId)
        print("Messages found:", EmailData.count())

        # if we have a record in the db, extract it from the mailbox
        if EmailData.count() > 0:
            print("Retrieving message", EmailData[0].mssg_number, "from mailbox @", mailboxPath)
            emailData = mailBox.get(EmailData[0].mssg_number)

            # if we have a message object, we parse it into JSON
            if emailData != None:
                context = parseMailboxMessage([(EmailData[0].mssg_number, emailData)])[0]
            else:
                success = 'false'
                message = 'unable to retrieve message from mailbox'

        else:
            success = 'false'
            message = 'no message found that matches the specified id'

    else:
        success = 'false'
        message = 'The URL must contain an id argument with the messages id'

    return render(request, 'mail/tmpEmlFull.html', context)



@csrf_exempt
def send(request, **args):
    print(args)
    print(request.POST)
    return HttpResponse('{"success":"false"}', content_type='application/json')


def parseMail(mailResults):
    '''Extracts various bits of data that's useful for the rendering of the mail items
    in on the page. Does nice things like evaluate the dates as a friendly period (minutes ago,
    days ago, last month, etc)'''
    revisedMail = []
    localTZ = optz.opTimezone(op.LOCALE['timezone_std'], op.LOCALE['timezone_dst'])
    today = dt.datetime.now(localTZ)

    for item in mailResults:
        eml = {}

        # # grab the date from the message and attempt to parse it
        # emailDateString = item.email_date
        # emailDateStruct = email.utils.parsedate_tz(emailDateString)
        # mailDate = email.utils.mktime_tz(emailDateStruct)
        # mailDateTime = dt.datetime.fromtimestamp(mailDate, localTZ)
        
        mailDateTime = item.email_date.astimezone(localTZ)
        dateDelta = today - mailDateTime
        
        if dateDelta.days < 1:# and today.day = item['date'].day:
            eml['email_date'] = str(int(mailDateTime.strftime("%I"))) + mailDateTime.strftime(":%M ") + mailDateTime.strftime("%p").lower()
        else:
            eml['email_date'] = str(int(mailDateTime.strftime("%d"))) + mailDateTime.strftime(" %b")

        # get the from data
        # fromName, fromAddr = email.utils.parseaddr(item.from_addr)
        if item.from_name != '':
            eml['from_name'] = item.from_name
        else:
            eml['from_name'] = item.from_addr

        # TODO: remove the rather kludgy and assuming header decoding. It assumes that the header will decode into a single
        #       entry with UTF-8 encoding.
        eml['subject'] = item.subject
        # eml['body'] = item.body.replace('<', '&lt;').replace(chr(10), '<br>')
        eml['body'] = cleanseHtmlForDisplay(item.body, 200) #.replace('<', '&lt;').replace(chr(10), '<br>')

        eml['mssg_id'] = item.mssg_id
        eml['mssg_number'] = item.mssg_number

        revisedMail.append(eml)
    return revisedMail


def parseMailboxMessage(mailResults):
    '''Extracts various bits of data that's useful for the rendering of the mail items
    in on the page. Does nice things like evaluate the dates as a friendly period (minutes ago,
    days ago, last month, etc)'''
    revisedMail = []
    localTZ = optz.opTimezone()
    today = dt.datetime.now(localTZ)

    for item in mailResults:

        eml = {}
        # grab the date from the message and attempt to parse it
        emailDateString = item[1]['Date']
        emailDateStruct = email.utils.parsedate_tz(emailDateString)
        mailDate = email.utils.mktime_tz(emailDateStruct)
        mailDateTime = dt.datetime.fromtimestamp(mailDate, localTZ)
        
        dateDelta = today - mailDateTime
        
        if dateDelta.days < 1:# and today.day = item['date'].day:
            eml['email_date'] = mailDateTime.strftime("%I:%M %p")
        else:
            eml['email_date'] = mailDateTime.strftime("%d %b")

        # get the from data
        fromName, fromAddr = email.utils.parseaddr(item[1]['From'])
        if fromName != '':
            eml['from_name'] = fromName
        else:
            eml['from_name'] = fromAddr

        # TODO: remove the rather kludgy and assuming header decoding. It assumes that the header will decode into a single
        #       entry with UTF-8 encoding.
        eml['subject'] = email.header.decode_header(item[1]['subject'])[0][0]

        # priorities are:
        #   text/html
        #   text/plain
        haveHtml = False
        for part in item[1].walk():
            if part.get_content_type() == 'text/html':
                bodyMessage = part.get_payload(decode=True)
                haveHtml = True
                print("using text/html")

            elif part.get_content_type() == 'text/plain' and not haveHtml:
                bodyMessage = part.get_payload(decode=True)
                print("using text/plain")

        eml['body'] = cleanseHtmlForDisplay(bodyMessage)  # removed this, so HTML renders in the browser  .replace('<', '&lt;').replace(chr(10), '<br>')

        eml['mssg_id'] = item[0]
        revisedMail.append(eml)
    return revisedMail

def cleanseHtmlForDisplay(htmlmessage, juststring=0):
    # print bodyMessage

    # using Beautiful Soup to remove potential mailicous elements
    # we parse text/plain as sometimes this will contain html...
    soup = BeautifulSoup(htmlmessage)

    scripts = soup('script')
    for s in scripts: s.clear()

    imgs = soup('img')
    for i in imgs: i['src'] = 'notsofast'

    refs = soup('a')
    for a in refs:
        a['target'] = '_blank'

    if juststring > 0:
        strlen = 0
        outstring = ''
        for string in soup.strings:
            outstring += string + ' '
            strlen += len(string)
            if strlen > juststring:
                break
        return outstring

    else:
        return soup.prettify()

def op_getinfo():
    return '''Sync your email with OmniPi and then use the integrated webmail app to read, write and manage your inbox.'''

# this is a closing comment