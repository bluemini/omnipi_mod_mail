from django import forms
from django.db import models

# Create your models here.
class Tag(models.Model):
    tag_value = models.TextField()


class Email(models.Model):

    mssg_number = models.TextField()			# assigned by the mailbox
    mssg_id = models.TextField(primary_key=True, unique=True) 	# as comes from the message

    from_addr = models.TextField()				# the value of the FROM header
    from_name = models.TextField(default="")	# parsed out 'name' from the FROM header

    to_addr = models.TextField()
    to_name = models.TextField(default="")

    cc_addr = models.TextField(default="")

    subject = models.TextField()
    body = models.TextField(blank=True)
    email_date = models.DateTimeField()

    thread_topic = models.TextField(blank=True, default="-")
    thread_index = models.TextField(blank=True, default="-")

    attachments = models.TextField(default="")

    # provide the tagging relationship
    tags = models.ManyToManyField(Tag, blank=True)

    # flag for synchronizing db and file
    sync_flag = models.IntegerField(default=0)


class Accounts(models.Model):
    account_name = models.CharField(max_length=100)

    def __str__(self):
        return '{} ({})'.format(self.account_name, self.id)

class AccountsForm(forms.ModelForm):
    class Meta:
        model = Accounts
        fields = ['account_name']

class MailSettings(models.Model):
    account = models.ForeignKey(Accounts, on_delete=models.CASCADE)
    incoming_mail_server = models.CharField(max_length=100)
    incoming_mail_username = models.CharField(max_length=100)
    incoming_mail_password = models.CharField(max_length=100)

    outgoing_mail_server = models.CharField(max_length=100)
    outgoing_mail_username = models.CharField(max_length=100)
    outgoing_mail_password = models.CharField(max_length=100)

class MailSettingsForm(forms.ModelForm):
    class Meta:
        model = MailSettings
        fields = ['account',
                  'incoming_mail_server', 'incoming_mail_username', 'incoming_mail_password',
                  'outgoing_mail_server', 'outgoing_mail_username', 'outgoing_mail_password']
        # labels = {
        #     'incoming_mail_server': _('Writer'),
        # }
        help_texts = {
            'incoming_mail_server': ('The name or IP address of your incoming mail server.'),
        }
        error_messages = {
            'incoming_mail_server': {
                'max_length': ('The name is too long for the db...sorry'),
            },
        }

    # def __init__(self, *args, **kwargs):
    #     # user = kwargs.pop('user','')
    #     super(MailSettingsForm, self).__init__(*args, **kwargs)
    #     # self.fields['user_defined_code']=forms.ModelChoiceField(queryset=UserDefinedCode.objects.filter(owner=user))
    #     self.fields['account'] = forms.ModelChoiceField(queryset=Accounts.objects.all())
