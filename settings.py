from django import forms

class MailSettings(forms.Form):
    incoming_mail_server = forms.CharField(label='Incoming Mail Server', max_length=100)
    incoming_mail_username = forms.CharField(label="Incoming server username")
    incoming_mail_password = forms.CharField(label="Incoming server password")

    outgoing_mail_server = forms.CharField(label="Outgoing Mail Server")
    outgoing_mail_username = forms.CharField(label="Outgoing server username")
    outgoing_mail_password = forms.CharField(label="Outgoing server password")

    # context['settings'] = [
    #     {
    #         "title": "mail_server",
    #         "name": "Mail Server",
    #         "type": "text",
    #         "value": "pop3.example.com",
    #         "validation": "url",
    #     }, {
    #         "title": "protocol_type",
    #         "name": "Mail Protocol",
    #         "type": "radio",
    #         "value": ["POP3", "IMAP"],
    #     }, {
    #         "title": "username",
    #         "name": "User Name",
    #         "type": "text",
    #         "value": "omnipi@example.com",
    #     }, {
    #         "title": "password",
    #         "name": "Password",
    #         "type": "password",
    #         "value": "0mniP1",
    #     }, {
    #         "title": "mail_signature",
    #         "name": "Signature",
    #         "type": "textarea",
    #         "value": "Sent from OmniPi - http://omnipi.org/",
    #     },
    # ]