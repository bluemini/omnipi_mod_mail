window.omnipi = window.omnipi || {};
window.omnipi.mail = window.omnipi.mail || {
	pageElements: ["emlInbox","emlFull","emlWrite"],
	pageRefresh: 300,								// check for new email every 5 minutes
	accounts: ["bluemini"],
	emailFullUrlStub: "/mail/fetch?id=",
	emailTemplateStub: "/static/mail/templates/"
};


// when the user selects the write new button, open the editor
$("#emlWriteNew").on("click", function(e) {
	$("#emlFull").addClass("show");
	e.preventDefault();
});

// listener for hitting the email close button
$("#emlNewClose").on("click", function(e) {
	$("#emlFull").removeClass("show");
	e.preventDefault();
});

// listener for hitting compose/reply/replyall/forward
$("#emlButtonCompose").on("click", function(e) {
	renderTemplate("tmpEmlWrite", {}, "emlWrite");
	window.history.pushState({
		app: 'omnipi',
		sub: 'mail',
		screen: 'write',
	}, 'write email', '#writemail');
});

// handlers for the compose screen
$("#emlTextTo").focus(function(e) {
	console.log("focus on TO");
});
$("#emlTextSubject").focus(function(e) {
	console.log("focus on SUBJECT");
});

// listener for the history pop event
window.onpopstate = function(e) {
	processState(e.state);
}

$("#emlInboxList").on("click", function(e) {
	var messageId = "",
		targetoi = e.target,
		keepgoing = true,
		passedImage = null;
	while (keepgoing) {
		// if we clicked on the image, keep track as this is a checkbox selector (not a view command)
		if (targetoi.className.indexOf('emlContactImg') >= 0) {
			passedImage = targetoi;
		}
		// as the event bubbles, try to extract the message id
		if (targetoi.id.substr(0, 3) === 'eml') {
			messageId = targetoi.id;
			keepgoing = false;
		} else {
			targetoi = targetoi.parentElement;
		}
	}
	// if we have the message id, then either show the email, or select it (based on previous events)
	if (messageId) {
		if (passedImage) {
			e.preventDefault();
			image = document.getElementById("emlCB" + messageId.substr(3));
			image.checked = !image.checked;
		} else {
			renderEmailFull(messageId);
		}
	}
});

/* given the message id (saved into the page history state) we can retrieve the message from the
server and render it into the full email display element and then show it. */
function renderEmailFull(messageId, preventPushState) {
	// fetch the email data
	var data = {};
	data.src = "/mail/email?id=" + encodeURIComponent(messageId.substr(3));
	
	renderTemplate("tmpEmlFull", data, "emlFull");

	if (!preventPushState) {
		window.history.pushState({
			app: 'omnipi',
			sub: 'mail',
			screen: 'read',
			id: messageId
		}, 'read email', '#'+encodeURIComponent(messageId.substr(3)) );
	}
}
function z__renderEmailFull(messageId, preventPushState) {
	// fetch the email data
	var emailDataUrl = omnipi.mail.emailFullUrlStub + encodeURIComponent(messageId.substr(3));
	var emailData = jQuery.ajax(emailDataUrl).done(function(data) {
		// get the template
		if (data.success) {

			renderTemplate("tmpEmlFull", data.email, "emlFull");

			// removed this custom rendering code and replaced with a library call
			// var tmp = $("#tmpEmlFull");
			// var tmpContent = parseTemplate(tmp, data.email);
			// var emailFull = $("#emlFull");
			// emailFull.html(tmpContent);
			// emlShow("emlFull");

			if (!preventPushState) {
				window.history.pushState({
					app: 'omnipi',
					sub: 'mail',
					screen: 'read',
					id: messageId
				}, 'read email', '#'+encodeURIComponent(messageId.substr(3)) );
			}
		}
	});
}


/* processState takes the state value from a popstate object and attempts to validate and 
load the correct page that the user has either asked for, or was looking at when they closed
the browser. */
function processState(pageState) {
	if (pageState) {
		console.log(pageState);
		if (pageState.app === 'omnipi' && pageState.sub === 'mail') {
			if (pageState.screen === 'read' && pageState.id) {
				renderEmailFull(pageState.id, true);
			}
		}
	} else if (window.location.pathname === '/mail/' && window.location.hash === "") {
		console.log('processState');
		emlShow('emlInbox');
	} else {
		console.log(window.location);
	}
}


/* currently this function knows too much about the application, which makes generalisation
of its function complex. The function needs to know which handlers to unlink as various pages
come and go, which needs to be either implied or defined outside of this logic 

// TODO: refactor this tight coupling out somehow
*/
function emlShow(emlSection) {
	// these are all the elements that we need to toggle between
	var emlElements = omnipi.mail.pageElements
	for (elem in emlElements) {
		var e = $("#"+emlElements[elem]);
		console.log(e);
		if (e !== null && e[0] && e[0].id === emlSection) {
			e.removeClass('unseen');

			// when viewing a full email, bind the back button
			if (emlSection === 'emlFull' || emlSection === 'emlWrite') {
				$("#emlButtonBack").on("click", function(e) {
					console.log("back button clicked");
					window.history.back();
				});

				// do something about the respond button being clicked
				$("#emlButtonRespond").on("click", function(e) {
					console.log("hitting respond should open up the full list of options");
					$("#emlRespondOptions").removeClass("unseen");
				});

				// do something about the delete button being clicked
				$("#emlButtonDelete").on("click", function(e) {
					console.log("hit the delete button");
					$("#emlButtonDelete").removeClass("unseen");
				});

				// set the frame to the rest of the window
				setEmlFrameHeight();
			}

			// bind the send button in the write screen
			if (emlSection === 'emlWrite') {

				// handler for the BACK button in the Write screen
				$("#emlWriteButtonBack").on("click", function(e) {
					console.log("back button clicked");
					window.history.back();
				});
				
				// handler for the SEND button in the write screen
				$("#emlWriteButtonSend").on("click", function(e) {
					console.log("Send button clicked");
					// assemble the data for the email in a JS object and send to a receiver
					var mailItem = {
						to: document.getElementById("emlWriteTextTo").value,
						subject: document.getElementById("emlWriteTextSubject").value,
						body: document.getElementById("emlWriteTABody").value
					};
					console.log(mailItem);
					$.ajax("/mail/send", {
						dataType: "json",
						data: mailItem,
						type: "POST"
					});
					window.history.back();
				});

				autoTextArea(document.getElementById('emlWriteTABody'), 30);
			}
		} else {
			e.addClass('unseen');
			// TODO: refactor this tight coupling out somehow
			if (emlSection === 'emlInbox') {
				$("#emlButtonBack").unbind("click");
				console.log(emlSection);
			}
		}
	}
}

function setEmlFrameHeight() {
	var ff = document.getElementById('emlFullFrame');
	if (ff) {
		console.log('doing resize');
		var newHeight = '' + (window.innerHeight - ff.offsetTop) + 'px';
		console.log(newHeight);
		ff.style.height = newHeight;
	}
}

$().ready(function() {
	processState(history.state);
	setEmlFrameHeight();

	$( window ).resize(setEmlFrameHeight);

	console.log('done this');
})


/*
Define the application in its own object omnipi[app]

Better framework around how to handle a single URL but deal with forward/back
	- user saves a deep url, ensure we can drop into that page
	- user saves a deep url, ensure that back works, even without a proper history
	- use of the state object, passed to pushState()

framework around showing and hiding elements in a page:
	- when an element has a button or target, how to bind/unbind from that element
		(example is the emlFull element, which is built based on the message id. Buttons 
		in the template must be bound to handlers, but when a different message is viewed
		those buttons are effectively destroyed and a whole new DOM element, containing
		the new message is created.)
	- keeping track of the areas of the page and the options for populating it.
		(we have just a single area in mail, and 3 views (inbox,read,compose), but how
		do we programmatically keep track of which is active and which ones we need to
		deactivate when we switch. Some kind of state machine?)

*/